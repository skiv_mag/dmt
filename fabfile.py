# coding: utf-8
import os.path as op
import os
from fabric.api import run, env, cd, roles, get


env.use_ssh_config = True
env.roledefs['production'] = ['ft']
project_name = "dmt"


def production_env():
    """Production enviroment"""
    env.key_filename = [op.join(os.environ['HOME'], '.ssh', 'id_rsa')]
    env.user = 'ubuntu'
    env.project_root = '/var/www/%s' % project_name
    # env.shell = '/usr/local/bin/bash -c'
    env.python = '/var/www/%s/env/bin/python' % project_name
    env.pip = '/var/www/%s/env/bin/pip' % project_name


@roles('production')
def deploy():
    production_env()
    with cd(env.project_root):
        run('git pull')
        run('sudo service uwsgi restart')


@roles('production')
def pip_install():
    production_env()
    run('{pip} install --upgrade -r {filepath}'.format(
        pip=env.pip,
        filepath=op.join(env.project_root, 'requirements.txt')
        )
        )


@roles('production')
def get_backup():
    production_env()
    backup_name = "current_%s.backup" % project_name
    run('pg_dump -w -U postgres -F c -f ~/{0} {1}'.format(backup_name, project_name))
    get('~/{0}'.format(backup_name), '~/backups/')
