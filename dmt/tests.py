
import json

from django.test import TestCase, RequestFactory

from dmt.models import Users, Rooms
from dmt.views import (
    get_models_list,
    get_fields,
    update_table,
    insert_row
)


class QueriesTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        Users.objects.create(
            name="John",
            surname="Doe",
            account_number="123",
            birthday="2013-01-01"
        )
        Rooms.objects.create(name=5, stage=1, bronned="2013-08-01")

    def test_get_models_list(self):
        req = self.factory.get('get_models_list')
        resp = get_models_list(req)
        # check if is valid requsest for models list
        self.assertEqual(resp.status_code, 200, msg="Check model list access")

    def test_get_fields(self):
        req = self.factory.get('get_fields')
        req.META['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest'
        resp = get_fields(req, model_name="users")
        # check if is valid requsest
        self.assertEqual(resp.status_code, 200, msg="Check table data access")
        # check if json valid
        self.assertRaises(
            json.loads(resp.content),
            msg="JSON validation from getting table data"
        )

    def test_update_table(self):
        data = {
            "table_name": 'rooms',
            "column": 'name',
            "new_value": 10,
            "row_id": 1
        }
        req = self.factory.post('update_table', data=data)
        req.META['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest'
        resp = update_table(req)
        # check if is valid requsest
        self.assertEqual(resp.status_code, 200, "Check update possibility")
        # check if is valid update for rooms
        updated_room = Rooms.objects.get(pk=1).name
        self.assertEqual(updated_room, 10, msg="Check table updating")

    def test_insert_row(self):
        data = {
            "table_name": 'rooms',
            "name": 20,
            "stage": 2,
            "bronned": "2013-12-08"
        }
        req = self.factory.post('update_table', data=data)
        req.META['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest'
        resp = insert_row(req)
        self.assertEqual(resp.status_code, 200, msg="Check if insert avaliable")
        return_data = json.loads(resp.content)
        inserted_room = Rooms.objects.get(pk=return_data["id"])
        self.assertIsNotNone(inserted_room, msg="Check if row inserted")
        self.assertEqual(inserted_room.stage, 2, msg = "Check if insert right")
