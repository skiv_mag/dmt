from django.conf.urls import patterns, url

urlpatterns = patterns('dmt.views',
    url(r'^$', 'get_models_list'),
    url(r'^update_table$', 'update_table'),
    url(r'^insert_row$', 'insert_row'),
    url(r'^(?P<model_name>[\w]+)/$', 'get_fields'),
)
