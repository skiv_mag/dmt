from django.contrib import admin
from models import Users, Rooms

# Register your models here.
admin.site.register(Users)
admin.site.register(Rooms)
