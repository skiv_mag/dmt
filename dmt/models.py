from django.db import models

import yaml
import os
from collections import defaultdict
from django.conf import settings


file_path = os.path.join(settings.BASE_DIR, 'dmt/models.yaml')

with open(file_path, 'r') as ya:
    yaml_file = yaml.load(ya.read())


MAP_TYPES = {
    'int': models.IntegerField,
    'char': models.CharField,
    'date': models.DateField
}



for yaml_model in yaml_file:

    model_name = yaml_model["title"]
    fields = defaultdict(None)
    for field in yaml_model["fields"]:
        field_type_class = MAP_TYPES[field["type"]]
        field_params = dict(verbose_name=field["title"])
        if field_type_class is models.CharField:
            field_params.update({"max_length" : 256})
        fields[field["id"]] = field_type_class(**field_params)
    fields.update({
        "__module__": __name__,
        "__unicode__": lambda self: unicode(self.name) if hasattr(self, "name") else self.__class__.__name__,
        "verbose_name": yaml_model["title"]
    })

    globals()[model_name] = type(model_name, (models.Model,), fields)
