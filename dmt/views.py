
import json

from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.db.models import get_models
from django.db.models.loading import get_model
from django.core.serializers.json import DjangoJSONEncoder
from django.forms.models import model_to_dict
from django.db.models import IntegerField, DateField, AutoField


def get_models_list(request):
    dmt_models = [
        m.__name__ for m in get_models() if m.__module__ == 'dmt.models']
    return render(request, "index.html", dict(models=dmt_models))


def get_fields(request, model_name):
    if request.is_ajax():
        model = get_model('dmt', model_name)
        field_names = []

        for f in model._meta.fields:
            if isinstance(f, DateField):
                field_type = 'date'
            elif isinstance(f, IntegerField):
                field_type = 'integer'
            elif isinstance(f, AutoField):
                continue
            else:
                field_type = 'string'
            field_names.append(
                {"label": f.verbose_name, "name": f.name, "type": field_type})

        data = [model_to_dict(m) for m in model.objects.all()]

        result = {"field_names": field_names, "table_data": list(data)}
        return HttpResponse(
            json.dumps(result, cls=DjangoJSONEncoder),
            content_type='application/json'
        )
    else:
        raise Http404


def update_table(request):
    if request.method == 'POST' and request.is_ajax():
        table = request.POST.get('table_name')
        column = request.POST.get('column')
        value = request.POST.get('new_value')
        row_id = request.POST.get('row_id')

        model = get_model('dmt', table)
        model_inst = model.objects.get(pk=row_id)
        setattr(model_inst, column, value)
        model_inst.save()

        return HttpResponse()
    else:
        raise Http404


def insert_row(request):
    if request.method == 'POST' and request.is_ajax():

        post_vars = request.POST.copy()

        table = post_vars.pop('table_name')[0]
        row = post_vars.dict()

        model = get_model('dmt', table)
        model_inst = model(**row)
        model_inst.save()

        return HttpResponse(
            json.dumps({"id": model_inst.id}),
            content_type='application/json'
        )
    else:
        raise Http404
