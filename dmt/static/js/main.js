$.ajaxSetup({ 
     beforeSend: function(xhr, settings) {
         function getCookie(name) {
             var cookieValue = null;
             if (document.cookie && document.cookie != '') {
                 var cookies = document.cookie.split(';');
                 for (var i = 0; i < cookies.length; i++) {
                     var cookie = jQuery.trim(cookies[i]);
                     // Does this cookie string begin with the name we want?
                 if (cookie.substring(0, name.length + 1) == (name + '=')) {
                     cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                     break;
                 }
             }
         }
         return cookieValue;
         }
         if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
             // Only send the token to relative URLs i.e. locally.
             xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
         }
     } 
});


DateCellEditor.prototype.displayEditor = function(element, htmlInput) 
{
    // call base method
    TextCellEditor.prototype.displayEditor.call(this, element, htmlInput);

    jQuery(htmlInput).datepicker({ 
        dateFormat: "yy-mm-dd",
        beforeShow: function() {
            // the field cannot be blurred until the datepicker has gone away
            // otherwise we get the "missing instance data" exception
            this.onblur_backup = this.onblur;
            this.onblur = null;
        },
        onClose: function(dateText) {
            // apply date if any, otherwise call original onblur event
            if (dateText != '') this.celleditor.applyEditing(htmlInput.element, dateText);
            else if (this.onblur_backup != null) this.onblur_backup();

        }
    }).datepicker('show');
};

DateCellValidator.prototype.isValid = function(value) 
{
    var iso = /\d{4}-[01]\d-[0-3]\d/;

    var parts = value.match(iso);

    if (parts == null) {
        return false;
    }

    return true;
};

var rule = new Object();

$(document).ready(function() {

    
    $("a").click(function (event) {
        event.preventDefault();

        var metadata = new Array();
        var data = new Array();
        

        $(".tablecontent table").empty();
        $("fieldset").remove();
        $('.new_user_button').show();
        href = $(this).attr("href");
        var request = $.get(href,
            function(model){

            $.each(model.field_names, function(i,field){

                metadata.push({ name: field.name, label: field.label, datatype: field.type, editable: true});
                
                var form_field_type = field.type;

                //translate date to text for Firefox and IE
                // because they doesn't support some htm5 input elements, including "date"
                //Add: jquery validate doesn't support number input type
                //TODO:Migrate to new version Firefox as soon as feature will be implimented
                
                $("#dialog-form").append('<label for="'+field.name+'">'+field.label+'</label>');
                $("#dialog-form").append('<input type="text" name="'+field.name+'" id="'+field.name+'" class="text ui-widget-content ui-corner-all">');
                if (field.type=='date'){
                  $("#dialog-form #"+field.name).addClass("date-input")
                  $("#dialog-form #"+field.name).attr("pattern", '/\d{4}-[01]\d-[0-3]\d/');
                };

                $("#dialog-form").append('<br><br>');

                //build validation role object
                
                if (field.type=='integer') {

                    rule[field.name] = {
                      digits: true,
                      required: true
                    }
                } 
                else if (field.type=='string'){
                    rule[field.name] = {
                      required: true
                    }
                }
                else if (field.type=='date'){
                    rule[field.name] = {
                      required: true,
                      ISODate:true
                    }
                }
                ;
            });
            $("#dialog-form").wrapInner('<form id="dialog-inner-form"><form />');
            $("#dialog-form").wrapInner('<fieldset />');

           

            $('input.date-input').datepicker({
                dateFormat: 'yy-mm-dd',
                onClose: function() {
                  $( this ).focus().blur();
                }
              });
            

            $.each(model.table_data, function(i,value){
                var id = value["id"];
                delete value["id"];
                data.push({"id":id,"values":value})
        });

        editableGrid = new EditableGrid("GridJsData", {
                "enableSort" : false, 
        });
        editableGrid.modelChanged = function(rowIndex, columnIndex, oldValue, newValue, row) {
            var data = {
                table_name:href,
                column : this.getColumnName(columnIndex),
                new_value : newValue,
                row_id : this.getRowId(rowIndex)

            }
            $.post( "update_table", data);

        };

        editableGrid.load({"metadata": metadata, "data": data});
        editableGrid.renderGrid("tablecontent", "tablegrid");



        });
        
    });

  $.validator.setDefaults({
                debug: true,
                success: "valid"
              });
  $.validator.addMethod(
      "ISODate",
      function(value, element) {
          return value.match(/\d{4}-[01]\d-[0-3]\d/);
      },
      "Please enter a date in the format yyyy-mm-dd."
  );
  

  $( "#dialog-form" ).dialog({
      autoOpen: false,
      height: 350,
      width: 400,
      modal: true,
      open: function(){
        $( "#dialog-inner-form" ).validate({
          // onfocusout: function(element) {
          //   this.element(element);
          // },
          rules: rule
        });
      },
      buttons: {
        "Create row": function() {

            isValid = $("#dialog-inner-form").valid();
            console.log(isValid);
            if(isValid){
              var fields = new Object();

              $.each($('input'), function(i,field){
                  if (field.type!="hidden") {
                    fields[field.id] = field.value;
                  };
              });

              fields["table_name"] = href;

              $.post("insert_row",fields)
                  .done(function(data) {
                      editableGrid.addRow(data.id, fields, true);
                      $( "#dialog-form" ).dialog().dialog( "close" );
                  })
                  .fail(function() {
                      alert( "Valid values expected" );
                  });
            }

            
              
        },
        Cancel: function() {
          $( this ).dialog( "close" );
        }
      },
      close: function() {
      }
    });

  $( "#create-user" )
        .button()
        .click(function() {
          $( "#dialog-form" ).dialog( "open" );
        });
});

