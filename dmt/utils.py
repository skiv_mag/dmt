
from pprint import pprint


def print_obj(object_name):
    try:
        pprint({x: getattr(object_name, x) for x in dir(object_name)})
    except:
        for x in dir(object_name):
            try:
                print {x: getattr(object_name, x)}
            except:
                'cant print %s' % x
