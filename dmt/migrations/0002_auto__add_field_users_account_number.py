# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Users.account_number'
        db.add_column(u'dmt_users', 'account_number',
                      self.gf('django.db.models.fields.CharField')(default='00000000', max_length=256),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Users.account_number'
        db.delete_column(u'dmt_users', 'account_number')


    models = {
        u'dmt.rooms': {
            'Meta': {'object_name': 'Rooms'},
            'bronned': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.IntegerField', [], {}),
            'stage': ('django.db.models.fields.IntegerField', [], {})
        },
        u'dmt.users': {
            'Meta': {'object_name': 'Users'},
            'account_number': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'birthday': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'surname': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        }
    }

    complete_apps = ['dmt']