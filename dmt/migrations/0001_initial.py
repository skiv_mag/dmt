# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Users'
        db.create_table(u'dmt_users', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('surname', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('birthday', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal(u'dmt', ['Users'])

        # Adding model 'Rooms'
        db.create_table(u'dmt_rooms', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.IntegerField')()),
            ('stage', self.gf('django.db.models.fields.IntegerField')()),
            ('bronned', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal(u'dmt', ['Rooms'])


    def backwards(self, orm):
        # Deleting model 'Users'
        db.delete_table(u'dmt_users')

        # Deleting model 'Rooms'
        db.delete_table(u'dmt_rooms')


    models = {
        u'dmt.rooms': {
            'Meta': {'object_name': 'Rooms'},
            'bronned': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.IntegerField', [], {}),
            'stage': ('django.db.models.fields.IntegerField', [], {})
        },
        u'dmt.users': {
            'Meta': {'object_name': 'Users'},
            'birthday': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'surname': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        }
    }

    complete_apps = ['dmt']